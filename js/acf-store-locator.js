var geocoder;
var map;
markers = [];

function doSearch(e) {
  if (typeof e == 'undefined' && window.event) { e = window.event; }
  if (e.keyCode == 13) {
    document.getElementById('acf-search-store').onclick();
  }
}

function codeAddress() {
  var address = document.getElementById('acf-address').value;
  geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      shopinfowindow.close();
      map.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location,
          icon: 'https://www.google.com/mapfiles/marker_yellow.png'
      });
      document.getElementById('acf-error').style.display = 'none';
    } else {
      document.getElementById('acf-error').style.display = 'block';
    }
  });
}


function handleNoGeolocation(errorFlag) {
  var options = {
    map: map,
    position: new google.maps.LatLng(39.707187,-101.615295)
  };

  map.setCenter(options.position);
  map.setZoom(4);
}

function initialize() {
  geocoder = new google.maps.Geocoder();

  var mapOptions = {
    zoom: 14,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  map = new google.maps.Map(document.getElementById('acf-map-canvas'), mapOptions);

  setMarkers(map, shops);

  shopinfowindow = new google.maps.InfoWindow({
    content: "Loading..."
  });

  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = new google.maps.LatLng(position.coords.latitude,
                                       position.coords.longitude);

      var positionmarker = new google.maps.Marker({
          map: map,
          position: pos,
          icon: 'https://www.google.com/mapfiles/marker_green.png'
      });

      map.setCenter(pos);


    }, function() {
      handleNoGeolocation(true);
    });
  } else {
    handleNoGeolocation(false);
  }
}

function setMarkers(map, shopmarkers) {
  for (var i = 0; i < shopmarkers.length; i++) {
    var shopinfo = shopmarkers[i];
    var siteLatLng = new google.maps.LatLng(shopinfo[1], shopinfo[2]);
    var marker = new google.maps.Marker({
        position: siteLatLng,
        map: map,
        title: shopinfo[0],
        html: shopinfo[3],
        type: shopinfo[4]
    });

    markers.push(marker);

    google.maps.event.addListener(marker, 'click', function() {
        shopinfowindow.setContent(this.html);
        shopinfowindow.open(map, this);
    });
  }
}

google.maps.event.addDomListener(window, 'load', initialize);