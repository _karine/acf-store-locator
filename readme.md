ACF Store Locator
=================

**Note:** only tested with WordPress 3.8 and ACF 4.3.4

Creates a 'Store' post type with custom fields using [Advanced Custom Fields](http://www.advancedcustomfields.com/).

## Usage

* Add stores
* Use shortcode `[acf-store-locator]` to insert the store locator in your content.

## Changelog

**1.1**

* Updated to work with WP 3.8 and ACF 4.3.4, older versions not supported

**1.0**

* Initial release
* Works with WP 3.6.1 and ACF 4.2.2